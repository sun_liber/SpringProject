package com.liber.sun.enums;

/**
 * code 和 msg的一一对应
 * Created by sunlingzhi on 2017/10/24.
 */
public enum ResultEnum {
    SUCCESS(0,"成功"),
    NO_USER(1,"No user"),
    USER_PASSWORD_NOT_MATCH(2,"user and password are not match"),
    NO_TOKEN(3,"Missing Token"),
    TOKEN_NOT_MATCH(4,"Token not match"),
    TOKEN_WRONG(5,"Token Wrong"),
    USER_EXIST(6,"user exsist"),
    ;
    private  Integer code;

    private String msg;

    ResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    //忽略Set方法，枚举往往不需要Set
    public Integer getCode() {
        return code;
    }
    public String getMsg() {
        return msg;
    }

}
