package com.liber.sun.service;

import com.liber.sun.dao.AdvanceRecordRepository;
import com.liber.sun.domain.AdvanceRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by sunlingzhi on 2018/3/12.
 */
@Service
public class AdvanceRecordService {
    @Autowired
    AdvanceRecordRepository advanceRecordRepository;

    public AdvanceRecord addAdvanceRecord(AdvanceRecord advanceRecord){
        return advanceRecordRepository.insert(advanceRecord);
    }
    public AdvanceRecord findAdvanceRecord(String id ){
        return advanceRecordRepository.findOne(id);
    }
    public void deleteAdvanceRecord(String id ){
        advanceRecordRepository.delete(id);
    }

}
