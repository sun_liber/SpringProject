package com.liber.sun.service;

import com.liber.sun.dao.RecordRepository;
import com.liber.sun.domain.Record;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by sunlingzhi on 2018/3/6.
 */
@Service
public class RecordService {
    @Autowired
    RecordRepository recordRepository;

    public Record addRecord(Record record){
        return recordRepository.insert(record);
    }
    public Record findRecord(String id){return recordRepository.findOne(id);}
    public void deleteRecord(String id){recordRepository.delete(id);}
}
