package com.liber.sun.service;

import com.liber.sun.dao.ServiceRepository;
import com.liber.sun.domain.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by sunlingzhi on 2018/3/5.
 */
@org.springframework.stereotype.Service
public class MServiceService {

    @Autowired
    ServiceRepository serviceRepository;

    public Service addService(Service service){
        return serviceRepository.insert(service);
    }
    public void deleteService(String id){
         serviceRepository.delete(id);
    }
    public Service updateService(Service service){
        return serviceRepository.save(service);
    }
    public Service findService(String id){
        return serviceRepository.findOne(id);
    }


    public Service [] findServiceByType(String type){
        return serviceRepository.findServicesByType(type);
    }
}
