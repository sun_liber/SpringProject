package com.liber.sun.service;

import com.liber.sun.dao.UserRepository;
import com.liber.sun.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by sunlingzhi on 2018/1/9.
 */
@Service
public class UserService {

    @Autowired
    private UserRepository userRepostitory;
    public List<User>  getAllUsers(){
        return userRepostitory.findAll();
    }
    public User findUserById(String id) {
        return userRepostitory.findOne(id);
    }
    public User findUserByName(String username) {
        return userRepostitory.findByUsername(username);
    }
    public void addUser(User user) {
        userRepostitory.insert(user);
    }
    public void updateUser(User user) {
        userRepostitory.save(user);
    }
    public void deleteUserById(String id) {
        userRepostitory.delete(id);
    }
}
