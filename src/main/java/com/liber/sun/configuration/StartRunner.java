//package com.liber.sun.configuration;
//
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.core.annotation.Order;
//import org.springframework.stereotype.Component;
//
///**
// * Created by sunlingzhi on 2017/12/18.
// */
////Spring Boot应用程序在启动后，会遍历CommandLineRunner接口的实例并运行它们的run方法。
//@Component
//@Order(value=1)//越大顺序越在前面
//public class StartRunner implements CommandLineRunner {
//    @Override
//    public void run(String... strings) throws Exception {
//        System.out.println(">>>>>>>>>>>>>>>服务启动执行，执行加载数据等操作111111111<<<<<<<<<<<<<");
//    }
//}
