//package com.liber.sun.configuration;
//
//import org.springframework.context.EnvironmentAware;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.env.Environment;
//
///**
// * Created by sunlingzhi on 2017/12/18.
// */
////凡是被spring管理的类，
//// 实现接口EnvironmentAware 重写方法 setEnvironment 可以在工程启动时，获取到系统环境变量
//@Configuration
//public class EnviromentConfig implements EnvironmentAware{
//
//    @Override
//    public void setEnvironment(Environment environment) {
//        System.out.println(environment.getProperty("JAVA_HOME"));
//    }
//}
