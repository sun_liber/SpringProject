//package com.liber.sun.configuration;
//
//import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
//import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
//import org.springframework.boot.web.servlet.ErrorPage;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Component;
//
///**
// * Created by sunlingzhi on 2018/1/10.
// */
//@Component
//public class VueConfiguration implements EmbeddedServletContainerCustomizer {
//    //当Vue的路由使用History模式的时候，如果URL匹配不到资源，就应该返回index.html页面
//    @Override
//    public void customize(ConfigurableEmbeddedServletContainer configurableEmbeddedServletContainer) {
//        configurableEmbeddedServletContainer.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND,"/index.html"));
//    }
//}
