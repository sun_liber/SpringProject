package com.liber.sun.support;

/**
 * Created by sunlingzhi on 2018/2/1.
 */
public class SysInfo {
    String hostname;
    String systemtype;
    String platform;
    String release;
    String uptime;
    String loadavg;
    String totalmem;
    String freemem;
    String cpus;
    String disk;

    public SysInfo() {
    }

    public SysInfo(String hostname, String systemtype, String platform, String release, String uptime, String loadavg, String totalmem, String freemem, String cpus, String disk) {
        this.hostname = hostname;
        this.systemtype = systemtype;
        this.platform = platform;
        this.release = release;
        this.uptime = uptime;
        this.loadavg = loadavg;
        this.totalmem = totalmem;
        this.freemem = freemem;
        this.cpus = cpus;
        this.disk = disk;
    }
}
