package com.liber.sun.support;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.concurrent.CopyOnWriteArrayList;
/**
 * Created by sunlingzhi on 2017/12/19.
 */
@Configuration
public class InstanceBeanConfigration {
    @Bean(name="instanceBean")
    public InstanceBean instanceBeanGenerate(){
        CopyOnWriteArrayList<Instance> instanceArrayList=new CopyOnWriteArrayList<>();
        return new InstanceBean(instanceArrayList);
    }
}
