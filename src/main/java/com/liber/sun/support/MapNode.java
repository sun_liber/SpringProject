package com.liber.sun.support;

/**
 * Created by sunlingzhi on 2018/3/5.
 */
public class MapNode {
    String type;

    public MapNode(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
