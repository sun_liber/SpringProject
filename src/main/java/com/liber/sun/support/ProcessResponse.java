package com.liber.sun.support;

/**
 * Created by sunlingzhi on 2018/3/6.
 */
public class ProcessResponse {
    String info;//进程打印的信息
    String operator;
    Boolean flag;

    public ProcessResponse(String info, String operator, Boolean flag) {
        this.info = info;
        this.operator = operator;
        this.flag = flag;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }
}
