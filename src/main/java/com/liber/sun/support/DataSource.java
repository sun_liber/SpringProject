package com.liber.sun.support;

/**
 * Created by sunlingzhi on 2018/3/8.
 */
public class DataSource {
    String filename;
    String filePath;
    Boolean isPrepared=false;

    public DataSource() {
    }

    public DataSource(String filename, String filePath) {
        this.filename = filename;
        this.filePath = filePath;
    }

    public DataSource(String filename, String filePath, Boolean isPrepared) {
        this.filename = filename;
        this.filePath = filePath;
        this.isPrepared = isPrepared;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Boolean getPrepared() {
        return isPrepared;
    }

    public void setPrepared(Boolean prepared) {
        isPrepared = prepared;
    }
}
