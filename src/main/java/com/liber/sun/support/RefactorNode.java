package com.liber.sun.support;


import com.liber.sun.utils.ProcessUtils;
import com.liber.sun.utils.XmlUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sunlingzhi on 2018/3/5.
 */
public class RefactorNode {
    String id;
    String method;
    int isFinished=0;//1成功 -1失败
    String logInfo="";
    List<DataSource> inputData;
    List<DataSource> outputData;



    public boolean CheckData() {
        for (DataSource dataSource : inputData) {
            File fileExists=new File(dataSource.getFilePath());
            if(!fileExists.exists()){
                dataSource.isPrepared=false;
                return false;
            }
            dataSource.isPrepared=true;
        }
        return true;
    }

    public ProcessResponse compute(String rootPath,String username) throws IOException, SAXException, ParserConfigurationException {
        //baseDir 工作目录
        String baseDir=rootPath+username+"/"+this.getId()+"/";
        File configFile = new File(baseDir + "cfg.xml");
        Document doc = XmlUtils.parseFile(configFile);
        NodeList nList = doc.getElementsByTagName("config");
        Element node = (Element) nList.item(0);
        String type = node.getElementsByTagName("type").item(0).getFirstChild().getNodeValue();
        String start = node.getElementsByTagName("start").item(0).getFirstChild().getNodeValue();

        String cmd = "";
        if (type.equals("exe")) {
            cmd = baseDir + start+" "+method;
        } else if (type.equals("jar")) {
            cmd = "java -jar " + baseDir + start+" "+method;
        }
        int lengthInput=this.getInputData().size();
        for (int i = 0; i < lengthInput; i++) {
            cmd+=" "+this.getInputData().get(i).getFilePath();
        }
        int lengthOutput=this.getOutputData().size();
        for (int i = 0; i < lengthOutput; i++) {
            cmd+=" "+this.getOutputData().get(i).getFilePath();
        }
        ProcessResponse processResponse= ProcessUtils.StartProcess(cmd,baseDir);

        for (int i = 0; i < this.outputData.size(); i++) {
            File file=new File(this.outputData.get(i).getFilePath());
            if(file.exists()){
                this.outputData.get(i).isPrepared=true;
            }
        }
        this.setLogInfo(processResponse.getInfo());
        return processResponse;
    }

    public RefactorNode() {
    }

    public RefactorNode(String id, String method, List<DataSource> inputData, List<DataSource> outputData) {
        this.id=id;
        this.method = method;
        this.inputData = inputData;
        this.outputData = outputData;
    }

    public RefactorNode(String id,String method,String [] inputs,String [] outputs) {
        List<DataSource> inputDataSource=new ArrayList<>();
        List<DataSource> outputDataSource=new ArrayList<>();
        for (int i = 0; i < inputs.length; i++) {
            inputDataSource.add(new DataSource(inputs[i].substring(inputs[i].lastIndexOf(File.separator)+1),inputs[i],true));
        }
        for (int i = 0; i < outputs.length; i++) {
            outputDataSource.add(new DataSource(outputs[i].substring(outputs[i].lastIndexOf(File.separator)+1),outputs[i]));
        }
        this.id=id;
        this.method = method;
        this.inputData = inputDataSource;
        this.outputData = outputDataSource;
    }

    public int getIsFinished() {
        return isFinished;
    }

    public void setIsFinished(int isFinished) {
        this.isFinished = isFinished;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public List<DataSource> getInputData() {
        return inputData;
    }

    public void setInputData(List<DataSource> inputData) {
        this.inputData = inputData;
    }

    public List<DataSource> getOutputData() {
        return outputData;
    }

    public void setOutputData(List<DataSource> outputData) {
        this.outputData = outputData;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogInfo() {
        return logInfo;
    }

    public void setLogInfo(String logInfo) {
        this.logInfo = logInfo;
    }
}
