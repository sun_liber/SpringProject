package com.liber.sun.support;

import java.util.Date;

/**
 * Created by sunlingzhi on 2018/2/1.
 */
public class Instance<T> {
    String guid;
    String sid;
    String sname;
    Date starttime;
    Integer status;    // 0正在运行 1运行完成，包括成功与失败 但是其实1是不存在的，因为如果成功或者失败的话，会将instance直接转换为record
    String username;
    T data;

    public Instance() {
    }

    public Instance(String guid, String sid, String sname, Date starttime, Integer status, String username, T data) {
        this.guid = guid;
        this.sid = sid;
        this.sname = sname;
        this.starttime = starttime;
        this.status = status;
        this.username = username;
        this.data = data;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
