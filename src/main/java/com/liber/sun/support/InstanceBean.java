package com.liber.sun.support;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by sunlingzhi on 2017/12/19.
 */
public class InstanceBean {

    CopyOnWriteArrayList<Instance> instanceArrayList;

    public InstanceBean() {
    }
    public InstanceBean(CopyOnWriteArrayList<Instance> instanceArrayList) {
        this.instanceArrayList = instanceArrayList;
    }

    public Boolean addInstance(Instance instance){
        for (Instance instance1 : this.instanceArrayList) {
            if(instance1.getGuid().equals(instance.getGuid())){
                return false;
            }
        }
        this.instanceArrayList.add(instance);
        return true;
    }

    public void removeByGuid(String guid){
        this.instanceArrayList.forEach(instance -> {
            if(instance.getGuid().equals(guid)){
                this.instanceArrayList.remove(instance);
                return;
            }
        });
    }

    //返回第一个instance
    public  Instance getInstanceByGuid(String guid){
        final Instance[] ins = {null};
        this.instanceArrayList.forEach(instance -> {
            if(instance.getGuid().equals(guid)){
                ins[0] =instance;
                return ;
            }
        });
        return  ins[0];
    }
    public  void changeStatus(String guid ,Integer status){
        this.instanceArrayList.forEach(instance -> {
            if(instance.getGuid().equals(guid)){
                instance.setStatus(status);
                return ;
            }
        });
    }

    public  Integer getStatusByGuid(String guid){
        final Integer[] integer = {null};
        this.instanceArrayList.forEach(instance -> {
            if(instance.getGuid().equals(guid)){
                integer[0] =instance.getStatus();
                return ;
            }
        });
        return  integer[0];
    }

    public  Integer getInstanceCount(){
        return instanceArrayList.size();
    }

    public CopyOnWriteArrayList<Instance> getInstanceArrayList() {
        return instanceArrayList;
    }


}
