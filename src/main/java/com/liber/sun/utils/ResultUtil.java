package com.liber.sun.utils;

import com.liber.sun.support.Result;
import com.liber.sun.enums.ResultEnum;

/**
 * Created by sunlingzhi on 2017/10/24.
 */
public class ResultUtil {

    public  static Result success(Object obj){
        Result result=new Result();
        result.setCode(ResultEnum.SUCCESS.getCode());
        result.setMsg(ResultEnum.SUCCESS.getMsg());
        result.setData(obj);
        return result;
    }

    //无返回对象的成功
    public  static  Result success(){
        return success(null);
    }

    public  static Result error(Integer code ,String msg){
        Result result=new Result();
        result.setCode(code);
        result.setMsg(msg);
        result.setData(null);
        return result;
    }
}
