package com.liber.sun.controller;


import com.liber.sun.service.AdvanceRecordService;
import com.liber.sun.service.RecordService;
import com.liber.sun.support.Instance;
import com.liber.sun.support.Result;
import com.liber.sun.domain.Service;
import com.liber.sun.service.MServiceService;
import com.liber.sun.support.InstanceBean;
import com.liber.sun.utils.MyFileUtils;
import com.liber.sun.utils.ResultUtil;
import com.liber.sun.utils.ZipUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;


/**
 * Created by sunlingzhi on 2017/10/30.
 */
@RestController
@RequestMapping("/common")
public class CommonController {

    @Value("${web.upload-path}services/")
    private String ROOT;

    @Autowired
    private InstanceBean instanceBean;
    @Autowired
    private MServiceService mServiceService;

    @Autowired
    private AdvanceRecordService advanceRecordService;
    @Autowired
    private RecordService recordService;
    /**
     *针对服务的运行实例
     */
    @ApiOperation(value = "获取运行实例", notes = "根据guid查询运行实例")
    @RequestMapping(value = "/instance", method = RequestMethod.GET)
    public Result getInstance(@RequestParam("guid") String guid) {
        return ResultUtil.success(instanceBean.getInstanceByGuid(guid));
    }

    @ApiOperation(value = "获取所有运行实例", notes = "根据guid查询运行实例")
    @RequestMapping(value = "/instances", method = RequestMethod.GET)
    public Result getAllInstance() {
        return ResultUtil.success(instanceBean.getInstanceArrayList());
    }


    /**
     *针对服务的运行结果
     */
    @ApiOperation(value = "获取运行结果", notes = "type可以为mapping refactor advanceRefactor")
    @RequestMapping(value = "/record", method = RequestMethod.GET)
    public Result getRecord(
                            @RequestParam("id") String id,
                            @RequestParam("type") String type) {
        if(type.equals("advanceRefactor")){
            return ResultUtil.success(advanceRecordService.findAdvanceRecord(id));
        }else{
            return ResultUtil.success(recordService.findRecord(id));
        }
    }



    @ApiOperation(value = "删除运行结果", notes = "type可以为mapping refactor advanceRefactor")
    @RequestMapping(value = "/recordDelete", method = RequestMethod.GET)
    public Result deleteRecord(@RequestParam("type") String type,
                               @RequestParam("id") String id
    ) {
        if(type.equals("advanceRefactor")){
            advanceRecordService.deleteAdvanceRecord(id);
            return ResultUtil.success("id: "+id+" 删除成功");
        }else{
            recordService.deleteRecord(id);
            return ResultUtil.success("id: "+id+" 删除成功");
        }
    }


    /**
     *针对服务的增删查改
     */
    @ApiOperation(value = "获取服务列表", notes = "type可以为mapping refactor \n")
    @RequestMapping(value = "/services", method = RequestMethod.GET)
    public Result getServiceList(@RequestParam("type") String type) {
        return ResultUtil.success(mServiceService.findServiceByType(type));
    }

    @ApiOperation(value = "根据id获取特定服务", notes = "")
    @RequestMapping(value = "/service", method = RequestMethod.GET)
    public Result getService(@RequestParam("id") String id) {
        return ResultUtil.success(mServiceService.findService(id));
    }

    @ApiOperation(value = "搜索特定服务", notes = "")
    @RequestMapping(value = "/searchServices", method = RequestMethod.GET)
    public Result searchService(@RequestParam("typeValue") String typeValue,
                                @RequestParam("value") String value) {
        // TODO: 2018/3/13 以后可以考虑做一个全局搜索 
        return ResultUtil.success("");
    }


    @ApiOperation(value = "上传服务,返回服务的ID", notes = "type可以为mapping refactor data visualization")
    @RequestMapping(value = "/uploadService", method = RequestMethod.POST)
    public Result uploadService(@RequestParam("type") String type,
                                          @RequestParam("name") String name,
                                          @RequestParam("description") String description,
                                          @RequestParam("author") String author,
                                          @RequestParam("snapshot") String snapshot,
                                          @RequestParam("portalId") String portalId,
                                          @RequestParam("file") MultipartFile file
    ) throws IOException {
        Service mService=new Service(null,
                type,name,description,author,snapshot,
                "",
                portalId,
                1,
                -1,
                -1,
                new Date());
        String id=mServiceService.addService(mService).getId();
        String basedir=ROOT+type+"/"+author+"/"+id+"/";
        ZipUtils.unZipFiles(MyFileUtils.multipartToFile(file),basedir);
        return ResultUtil.success(id);
    }

    @ApiOperation(value = "删除服务", notes = "")
    @RequestMapping(value = "/deleteService", method = RequestMethod.GET)
    public Result deleteService(@RequestParam("id") String id
    ) {
        mServiceService.deleteService(id);
        return ResultUtil.success("删除成功");
    }



    @ApiOperation(value = "更新服务的具体详情", notes = "")
    @RequestMapping(value = "/updateService", method = RequestMethod.GET)
    public Result saveService(@RequestParam("Service") Service service) {
        return ResultUtil.success(mServiceService.updateService(service));
    }


}
