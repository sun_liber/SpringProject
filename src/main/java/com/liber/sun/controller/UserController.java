package com.liber.sun.controller;


import com.liber.sun.domain.User;
import com.liber.sun.enums.ResultEnum;
import com.liber.sun.exception.MyException;
import com.liber.sun.service.UserService;
import com.liber.sun.support.Result;
import com.liber.sun.utils.JwtUtils;
import com.liber.sun.utils.MyFileUtils;
import com.liber.sun.utils.ResultUtil;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by sunlingzhi on 2017/10/19.
 */
@RestController //默认返回json格式
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @Value("${web.upload-path}filemanager/")
    private String ROOT;

    @ApiOperation(value = "获取用户列表", notes = "")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result<List> getUserList() {
        List<User> r = userService.getAllUsers();
        return ResultUtil.success(r);
    }

    @ApiOperation(value = "获取用户详细信息", notes = "根据url的id来获取用户详细信息")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public Result<User> getUserByUsername(@RequestParam("username") String username) {
        // 处理"/users/{id}"的GET请求，用来获取url中id值的User信息
        // url中的id可通过@PathVariable绑定到函数的参数中
        User user = this.userService.findUserByName(username);
        if (user == null) {
            throw new RuntimeException("用户名为空");
        }
        return ResultUtil.success(user);
    }

    @ApiOperation(value = "更新用户详细信息", notes = "根据url的id来指定更新对象，并根据传过来的user信息来更新用户详细信息")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Result<String> updateUser(       @RequestParam("id") String id,
                                            @RequestParam("password") String password,
                                            @RequestParam("username") String username,
                                            @RequestParam("tags") String [] tags
    ) {
        try {
            userService.updateUser(new User(id, username, password,tags));
            return ResultUtil.success("更新成功");
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

    }

    @RequestMapping(value = "/doLogin", method = RequestMethod.POST)
    public Result<String> doLogin(@RequestParam("username") String username,
                                  @RequestParam("password") String password ) {
        User user =userService.findUserByName(username);
        if(user==null){
            throw new MyException(ResultEnum.NO_USER);
        }else{
            if(user.getPassword().equals(password)){
                String jwtToken= JwtUtils.generateToken(username, password);
                return ResultUtil.success("Bearer"+" "+jwtToken);
            }else{
                throw new MyException(ResultEnum.USER_PASSWORD_NOT_MATCH);
            }
        }
    }

    @RequestMapping(value = "/doRegister", method = RequestMethod.POST)
    public Result<User> doRegister(@Valid User user) {
        if(userService.findUserByName(user.getUsername())!=null){
            throw new MyException(ResultEnum.USER_EXIST);
        }else{
            userService.addUser(user);
            //添加一个用户 为其创建一个文件夹
            MyFileUtils.mkFolder(ROOT+user.getUsername());
            return ResultUtil.success(user);
        }
    }




}




