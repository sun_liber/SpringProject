package com.liber.sun.controller;


import com.liber.sun.support.*;
import com.liber.sun.domain.Record;
import com.liber.sun.service.RecordService;
import com.liber.sun.utils.ProcessUtils;
import com.liber.sun.utils.ResultUtil;
import com.liber.sun.utils.XmlUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

/**
 * Created by sunlingzhi on 2018/2/1.
 */
@RestController //默认返回json格式
@RequestMapping("/datamap")
public class MapController {

    @Value("${web.upload-path}services/datamap/")
    private String ROOT;

    @Autowired
    private InstanceBean instanceBean;
    @Autowired
    private RecordService recordService;

    @ApiOperation(value = "调用映射服务", notes = "callType的值是src2udx或者udx2src      返回 服务实例的 guid")
    @RequestMapping(value = "/use/call", method = RequestMethod.GET)
    public Result dataMapRun(@RequestParam("id") String id,
                                     @RequestParam("name") String name,
                                     @RequestParam("in_file") String in_file,
                                     @RequestParam("out_file") String out_file,
                                     @RequestParam("username") String username,
                                     @RequestParam("callType") String callType
    ) throws IOException, ParserConfigurationException, SAXException {
        MapNode mapData = new MapNode(callType);
        UUID uuid = UUID.randomUUID();
        Instance instance = new Instance<>(uuid.toString(), id, name, new Date(), 0, username, mapData);
        instanceBean.addInstance(instance);
        String baseDir = ROOT + username + "/" + id + "/";
        File configFile = new File(baseDir + "cfg.xml");
        Document doc = XmlUtils.parseFile(configFile);
        NodeList nList = doc.getElementsByTagName("config");
        Element node = (Element) nList.item(0);
        String type = node.getElementsByTagName("type").item(0).getFirstChild().getNodeValue();
        String start = node.getElementsByTagName("start").item(0).getFirstChild().getNodeValue();
        String cmd = "";
        if (type.equals("exe")) {
            cmd = baseDir + start;
        } else if (type.equals("jar")) {
            cmd = "java -jar " + baseDir + start;
        }

        if (callType.equals("src2udx")) {
            cmd += " -r -f " + in_file + " -u " + out_file; // src -> udx
        } else if (callType.equals("udx2src")) {
            cmd += " -w -u " + in_file + " -f " + out_file; // udx -> src
        }
        ProcessResponse processResponse=ProcessUtils.StartProcess(cmd,baseDir);
        instanceBean.removeByGuid(uuid.toString());
        Record record=new Record(null,"mapping",id,new Date(),cmd,processResponse.getInfo(),processResponse.getFlag());
        return ResultUtil.success(recordService.addRecord(record));
    }


    @ApiOperation(value = "调用映射服务", notes = "callType的值是src2udx或者udx2src      返回 服务实例的 guid")
    @RequestMapping(value = "/use/udxSchema", method = RequestMethod.GET)
    public Result<String> getSchema(@RequestParam("id") String id,
                                    @RequestParam("in_oid") String in_oid,
                                    @RequestParam("in_filename") String in_filename,
                                    @RequestParam("out_dir") String out_dir,
                                    @RequestParam("out_filename") String out_filename,
                                    @RequestParam("username") String username,
                                    @RequestParam("callType") String callType
    ) {
        return ResultUtil.success("");
    }

    @ApiOperation(value = "调用映射服务", notes = "callType的值是src2udx或者udx2src      返回 服务实例的 guid")
    @RequestMapping(value = "/use/getNode", method = RequestMethod.GET)
    public Result<String> getNode(@RequestParam("id") String id,
                                  @RequestParam("in_oid") String in_oid,
                                  @RequestParam("in_filename") String in_filename,
                                  @RequestParam("out_dir") String out_dir,
                                  @RequestParam("out_filename") String out_filename,
                                  @RequestParam("username") String username,
                                  @RequestParam("callType") String callType
    ) {
        return ResultUtil.success("");
    }
}
