//package com.liber.sun.controller;
//
//
//import com.liber.sun.support.Result;
//import com.liber.sun.support.SysInfo;
//import com.liber.sun.utils.ResultUtil;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
///**
// * Created by sunlingzhi on 2018/2/1.
// */
//@RestController
//@RequestMapping("/sys")
//public class SystemController {
//
//    @RequestMapping(value = "/info", method = RequestMethod.GET)
//    public Result getSysInfo() {
//        System.out.println(Runtime.getRuntime().freeMemory());
//        SysInfo sysInfo=new SysInfo();
//        return ResultUtil.success(sysInfo);
//    }
//
//}
