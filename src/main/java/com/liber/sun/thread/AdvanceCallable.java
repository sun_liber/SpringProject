package com.liber.sun.thread;

import com.liber.sun.support.RefactorNode;

import java.util.concurrent.Callable;

/**
 * Created by sunlingzhi on 2018/3/8.
 */
public class AdvanceCallable implements Callable<RefactorNode>{

    private AdvanceHandle advanceHandle;
    private int index;

    public AdvanceCallable(AdvanceHandle advanceHandle, int index) {
        this.advanceHandle = advanceHandle;
        this.index = index;
    }

    //这个线程的工作就是运行索引为Index的节点
    @Override
    public RefactorNode call() throws Exception {
        return this.advanceHandle.runNode(index);
    }
}
