//package com.liber.sun.aspect;
//
//
//import com.liber.sun.domain.User;
//import com.liber.sun.enums.ResultEnum;
//import com.liber.sun.exception.MyException;
//import com.liber.sun.service.UserService;
//import com.liber.sun.utils.JwtUtils;
//import io.jsonwebtoken.Claims;
//import org.aspectj.lang.annotation.*;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import org.springframework.web.context.request.RequestContextHolder;
//import org.springframework.web.context.request.ServletRequestAttributes;
//
//import javax.servlet.http.HttpServletRequest;
//
///**
// * Created by sunlingzhi on 2017/10/24.
// */
//@Aspect
//@Component
//public class NeedTokenAspect {
//
//    private final static Logger logger = LoggerFactory.getLogger(NeedTokenAspect.class);
//
//
//    @Autowired
//    UserService userService;
//
//    //定义切点
//    @Pointcut("execution(public * com.liber.sun.controller.FileManagerController.*(..))")     //拦截的所有方法
//    public void point() {
//    }
//
//    @Before("point()")
//    public void doBeforePoint() {
//        ServletRequestAttributes attributes =
//                (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//        HttpServletRequest request = attributes.getRequest();
//        String auth = request.getHeader("Authorization");
//        if (auth == null || !auth.startsWith("Bearer ")) {
//            throw new MyException(ResultEnum.NO_TOKEN);
//        } else {
//            Claims claims = JwtUtils.parseJWT(auth);
//            if (claims == null) {
//                throw new MyException(ResultEnum.TOKEN_WRONG);
//            } else {
//                User user = userService.findUserByName((String) claims.get("username"));
//                if (user.getPassword().equals((String) claims.get("password"))) {
//                    logger.info("url={}", request.getRequestURI());
//                    logger.info("ip={}", request.getRemoteAddr());
//                } else {
//                    throw new MyException(ResultEnum.TOKEN_NOT_MATCH);
//                }
//            }
//
//        }
//    }
//
//    @After("point()")
//    public void doAfterPoint() {
//        logger.info("<=======>Okay<=======>");
//    }
//
//
//    //得到Http请求返回的东西，往往是一个Object，需要toString()方法以显示内容
//    @AfterReturning(pointcut = "point()", returning = "object")
//    public void doAfterReturning(Object object) {
//        logger.info("response={}", object.toString());
//    }
//
//
//}
