package com.liber.sun.domain;

import org.springframework.data.annotation.Id;

import java.util.Date;

/**
 * Created by sunlingzhi on 2018/2/28.
 */
public class Record {

    @Id
    private String id;

    private String type;//mapping 和refactor
    private String serviceId;
    private String operator;
    private String logInfo;
    private Date date;
    private Boolean status;

    public Record() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getLogInfo() {
        return logInfo;
    }

    public void setLogInfo(String logInfo) {
        this.logInfo = logInfo;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Record(String id, String type, String serviceId, Date date,String operator, String logInfo, Boolean status) {
        this.id = id;
        this.type = type;
        this.serviceId = serviceId;
        this.operator = operator;
        this.logInfo = logInfo;
        this.date = date;
        this.status = status;
    }
}
