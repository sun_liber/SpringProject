package com.liber.sun.domain;

import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by sunlingzhi on 2018/1/9.
 */
@Document(collection = "User")
public class User {
    @Id
    private String id;

    private String username;

//    @Length(min=3,max=6,message = "用户密码不能少于3,大于6")
    private String password;

    private String [] tags;

    public User() {
    }

    public User(String id, String username, String password, String[] tags) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.tags = tags;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String[] getTags() {
        return tags;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }
}
