package com.liber.sun.domain;

import org.springframework.data.annotation.Id;

import java.util.Date;

/**
 * Created by sunlingzhi on 2018/2/28.
 */
public class Service {

    @Id
    private String id;

    private String type;
    private String name;
    private String description;
    private String author;
    private String snapshot;
    private String details;
    private String portalId;

    private int availableFlag;
    private int registerFlag;
    private int deleteFlag;

    private Date date;

    public Service() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSnapshot() {
        return snapshot;
    }

    public void setSnapshot(String snapshot) {
        this.snapshot = snapshot;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getPortalId() {
        return portalId;
    }

    public void setPortalId(String portalId) {
        this.portalId = portalId;
    }

    public int getAvailableFlag() {
        return availableFlag;
    }

    public void setAvailableFlag(int availableFlag) {
        this.availableFlag = availableFlag;
    }

    public int getRegisterFlag() {
        return registerFlag;
    }

    public void setRegisterFlag(int registerFlag) {
        this.registerFlag = registerFlag;
    }

    public int getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(int deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Service(String id, String type, String name, String description, String author, String snapshot, String details, String portalId, int availableFlag, int registerFlag, int deleteFlag, Date date) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.description = description;
        this.author = author;
        this.snapshot = snapshot;
        this.details = details;
        this.portalId = portalId;
        this.availableFlag = availableFlag;
        this.registerFlag = registerFlag;
        this.deleteFlag = deleteFlag;
        this.date = date;
    }
}
