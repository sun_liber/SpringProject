package com.liber.sun.domain;

import com.liber.sun.support.RefactorNode;
import org.springframework.data.annotation.Id;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

/**
 * Created by sunlingzhi on 2018/3/9.
 */
public class AdvanceRecord {
    @Id
    private String id;
    private Date date;
    private List<RefactorNode> nodeList;

    public AdvanceRecord() {
    }

    public AdvanceRecord(String id, Date date, List<RefactorNode> nodeList) {
        this.id = id;
        this.date = date;
        this.nodeList = nodeList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<RefactorNode> getNodeList() {
        return nodeList;
    }

    public void setNodeList(List<RefactorNode> nodeList) {
        this.nodeList = nodeList;
    }
}
