package com.liber.sun.dao;

import com.liber.sun.domain.AdvanceRecord;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by sunlingzhi on 2018/3/12.
 */
public interface AdvanceRecordRepository extends MongoRepository<AdvanceRecord,String> {
}
