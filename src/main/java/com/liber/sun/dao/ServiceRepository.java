package com.liber.sun.dao;

import com.liber.sun.domain.Service;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by sunlingzhi on 2018/3/5.
 */
public interface ServiceRepository extends MongoRepository<Service,String> {
    public Service [] findServicesByType(String type);
}
