package com.liber.sun.dao;

import com.liber.sun.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by sunlingzhi on 2018/1/9.
 */
public interface UserRepository extends MongoRepository<User,String> {
    public User findByUsername(String username);
}


