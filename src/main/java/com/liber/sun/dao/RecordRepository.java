package com.liber.sun.dao;

import com.liber.sun.domain.Record;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by sunlingzhi on 2018/3/6.
 */
public interface RecordRepository  extends MongoRepository<Record,String> {
}
